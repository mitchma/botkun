import src.data as data
from src.mob import mob

def squeeze(val, min_val, max_val):
    if val > max_val:
        return max_val
    elif val < min_val:
        return min_val
    return val

def final_mult(stats):
    wep = wep_mult(stats)
    pm_atk = pm_atk_mult(stats)
    dmg = dmg_mult(stats)
    per_dmg = per_dmg_mult(stats)
    piercing = piercing_mult(stats)
    pm_piercing = pm_piercing_mult(stats)
    crit = crit_mult(stats)
    final_mult = wep * pm_atk * per_dmg * dmg * piercing * pm_piercing \
                 * crit
    return final_mult

def wep_mult(stats):
    wep = stats["wep"]
    wa = wep["atk"]
    coeff = 1.0 if wep["rarity"] in ("normal", "rare", "exceptional") \
            else data.wep_coeff[wep["rarity"]][stats["job"]]
    b_atk = stats["attr"]["b_atk"] + pet_b_atk(stats)
    return wa + coeff * b_atk

def pet_b_atk(stats):
    atk_per_level = data.pet_rarity_to_atk[stats["pet"]["rarity"]]
    return atk_per_level * stats["pet"]["level"] * data.PET_BONUS_ATK_RATIO

# TODO: maybe include tonics/candles/whetstones, though these aren't gear.
def pm_atk_mult(stats):
    def wep_effective_mult():
        for wep_id in data.wep_effect:
            if stats["gear_effect"][wep_id]:
                if data.wep_effect[wep_id]["type"] == "pm_atk":
                    return effective_mult(wep_id, vs = stats["option"]["vs"])
                break # only first active weapon effect considered
        return 1.00

    pm_atk = stats["attr"]["pm_atk"]
    wep_effect = wep_effective_mult()
    kand_mult = effective_mult("kandura_pendant") \
                if stats["gear_effect"]["kandura_pendant"] else 1.00
    katvan_horns = effective_mult("katvan_horns") \
                   if stats["gear_effect"]["katvan_horns"] else 0.00
    varr_horns = effective_mult("varrekant_horns") \
                 if stats["gear_effect"]["varrekant_horns"] else 1.00

    # assuming additive weapon effect, kandura, varrekant horns
    mult = (pm_atk + katvan_horns) \
           * (wep_effect + kand_mult + varr_horns - 2.00)
    return mult

# Damage multipier (effects boosting the generic 'damage' term used ingame are
# applied here)
def dmg_mult(stats):
    def wep_effective_mult():
        for wep_id in data.wep_effect:
            if stats["gear_effect"][wep_id]:
                if data.wep_effect[wep_id]["type"] == "dmg":
                    return effective_mult(wep_id, vs = stats["option"]["vs"])
                break # only first active weapon effect considered
        return 1.00

    wep = wep_effective_mult()
    return wep

# Calculate a gear's overall effect with perfect uptime
def effective_mult(gear_id, vs = None):
    gear = data.wep_effect[gear_id] if gear_id in data.wep_effect \
           else data.non_wep_effect[gear_id]
    uptime = gear["duration"] / gear["cooldown"]
    mult = gear["mult"]
    # quadruple effect in Shadow Altar, Moonlight Fortress, Clock Tower
    if gear_id in ("msl", "murp", "rune") and vs in ("cdev", "cmoc", "cpap"):
        mult = 1 + (mult - 1) * 4
    elif gear_id in ("rage", "rage_double") and vs == "dark_descent":
        mult = 1 + (mult - 1) * 2
    mult = mult * uptime + (1 - uptime)
    return mult

# Percent damage multiplier
def per_dmg_mult(stats):
    attr = stats["attr"]
    per_dmg = 100 + attr["mr_dmg"] + attr["ele_dmg"] + attr["boss_dmg"]
    dmg_tot_per = attr["dmg_tot"]
    per_dmg = per_dmg + dmg_tot_per if stats["option"]["dmg_tot_add"] \
              else per_dmg * (1.0 + dmg_tot_per / 100.0)
    return per_dmg / 100.0

def piercing_mult(stats):
    piercing = squeeze(stats["attr"]["piercing"], 0.0, 30.0)
    if stats["gear_effect"]["varrekant_wings"]:
        varr_wings = data.non_wep_effect["varrekant_wings"]
        uptime = varr_wings["duration"] / varr_wings["cooldown"]
        enhanced = squeeze(piercing + varr_wings["mult"], 0.0, 30.0) / 100.0
    else:
        uptime, enhanced = 0.0, 0.0

    piercing /= 100.0
    mult = 1 / (1.0 - enhanced) * uptime + 1 / (1.0 - piercing) * (1.0 - uptime)
    return mult

def pm_piercing_mult(stats):
    resist = mob[stats["option"]["vs"]]["phys_resist"]
    pm_piercing = stats["attr"]["pm_piercing"]
    remaining_armor = max(0.0, resist - pm_piercing * 15)
    return (1500.0 - remaining_armor) / 1500.0

def crit_mult(stats):
    chance = crit_chance(stats)
    crit_dmg_mult = stats["attr"]["crit_dmg"] / 100.0
    crit_mult = chance * crit_dmg_mult + (1.0 - chance)
    return crit_mult

def crit_chance(stats):
    crit_eva = mob[stats["option"]["vs"]]["crit_eva"]
    crit_coeff = data.job[stats["job"]]["luk_to_crit"]
    luk = stats["stats"]["luk"]
    crit_rate = stats["attr"]["crit_rate"]
    chance = (crit_coeff * luk + 5.3 * crit_rate) / (crit_eva * 2.0) \
             * data.CORRECT_CRITICAL
    chance = min(chance, data.CRIT_CHANCE_CAP)

    if stats["job"] == "assassin":
        FS_COOLDOWN = 60.0
        fs_level = stats["skill"]["fatal_strikes"]
        uptime = data.skill["fatal_strikes"][fs_level] / FS_COOLDOWN
        chance = uptime + (1.0 - uptime) * chance

    return chance
