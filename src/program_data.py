import src.util as util

help_compare = \
"""
Compare different combinations of stats against your current stats. Each line is a set of stat combinations. Most values are added onto the current character stats and the new multiplier will be compared with the current multiplier to get a damage percent differential. Skill level and pet level are not additive and will be set as the new value.

Which stats are best for you?
!compare
piercing: 1
pm_piercing: 1
crit_dmg: 160
str: 16
pm_atk: 12, piercing: 1, piercing: 2, piercing: -0.5

You currently have a 1.3% piercing/3% boss damage Absolute Necklace with a tier 3 offense gem and two empty sockets equipped. You want to see if you should put some gems into the Absolute Necklace or use a Kandura's Pendant instead.
!compare
kandura_pendant: true, piercing: -1.3, boss_dmg: -3, main_stat_gem: -3
main_stat_gem: 7, offense_gem: 5

Want to compare with your friends (comparison is meaningless if jobs or skill builds are different)?
!compare
@FRIEND_A
@FRIEND_B, piercing: 5
piercing: 2.5, @MYSELF, piercing: 2.5
piercing: 5

You're worried that your bad legendary weapon is less effective than your Murp weapon?
!compare
wep_rarity: legendary, wep_atk: -1000, wep_effect: panic_double, piercing: -1, pm_piercing: -11.2, int: 14

Let's see if it is worth it to go from a good exceptional pet to a bad epic pet.
!compare
pet_rarity: epic, pet_level: 50, piercing: -2, pm_piercing: -3, piercing: 0.8, boss_dmg: 0.8

Different enemies have different resists. You'll need 16.6% physical or magic piercing for Cdev while only 10% for Infernog, so check if you actually need the extra stats.
!compare
vs: cdev, pm_piercing: 1
vs: infernog, pm_piercing: 1

You want to reroll into a different class?
!compare
job: assassin, fatal_strikes: 6

Possibility is endless. Play around with it. `!info compare_key` for all possible keys.
"""

help_crit_chance = \
"""
The critical hit chance that is hardcapped at 40%. Your critical chance may change depending on the enemy you are fighting due to the different critical evasion rate each mob has. As of the 2018 December update, all mobs have 50 critical evade. In future contents, mobs will have higher critical evasion, rendering crit builds inefficient on many classes. If you can stack a ton of crit rate and damage, crit builds may be far better than piercing as they can surpass the 1.4x multiplier cap of piercing.

To view your own crit chance, simply invoke `!crit_chance` (or `!crit_chance @YOUR_NAME`)
To view your friend's crit chance, invoke `!crit_chance @MY_FRIEND`
"""

help_help = \
"""
To start, grab the template from `!template` and set your character stats with `!set`.
Read up on `!info limitation` to know what the calculator can or can't do.
To go to a new line, press `shift` + `enter`.

Commands:

help:            Displays this message
help <COMMAND>:  More detailed command explanation, ie. `!help set`
info:            General game information, credits.

compare:         Compare stat combinations.
crit_chance:     Critical hit chance.
mob:             Mob information.
multiplier:      Numerical rating of your stats from Vegeta's scouter. Used for comparisons.
opt_attr:        Optimal stat distribution between main stat and critical rate.
opt_gems:        Optimal distribution between main stat and offense gems.
rant:            Waaaahhh!
remove:          Remove your character's stats.
set:             Set character's stats (values from in-game advanced stats page).
stats:           View character's current stats.
template:        Provide a template used to set character stats.

If you need more help, add me on Discord M&Ms#1839 or whisper/mail Refraction on NAW.
"""

help_help_help = \
"""
You think you're funny?
"""

help_info = \
"""
Arguments:

compare_key:  View all valid keys used in `!compare`.
credit:       Sources.
dim_ret:      Information on diminishing returns.
limitation:   Limitations about the program.
wep_effect:   View all valid weapon effects.
what_cape:    Information on Absolute Cape, Varrekant's Wings, and Balrog Wings.
what_gem:     Information on main stat and offense gems.
what_neck:    Information on Absolute Necklace and Kandura's Pendant.
what_wep:     Information on double piercing weapon.

For example, to view the limitations, use `!info limitation`.
"""

help_mob = \
"""
Query the mobs known to the Bot with `!mob <MOB_ID>`. Currently, the `<MOB_ID>` can be one of the following:
{}
""".format(util.mob_ids())

help_multiplier = \
"""
A numerical rating of the stats. The higher the better. This number may change depending on the enemy and may differ greatly between classes.
To view your own multiplier, simply invoke `!multiplier` (or `!multiplier @YOUR_NAME`)
To view your friend's multiplier, invoke `!multiplier @MY_FRIEND`
"""

help_opt_attr = \
"""
Optimize attribute distribution between main stat and critical rate. This may be useful in the future as we have more access to critical rate.

To use this, reset your attribute points, and update your character stats using `!set`.
Invoke `!opt_attr <AMNT>` where <AMNT> is the number of attribute points:

!opt_attr 69
"""

help_opt_gems = \
"""
Make sure to read about gems using `!info what_gem` first.

Optimal distribution between main stat and offense gems. The three optional parameters are:
- amount: number of gems to socket, 0 to 9.
- tier: gem tier, -10 to 10. Negative means taking gem off. Use `!compare` for per-gem tiers.
- offset: stats to be applied before gems are socketed. Works just like `!compare`. This is useful for emulating 'taking off' your gems (ie. offense_gem: -6) before optimizing from a clean slate.

You want to see how tier 5 main stat gems compete with tier 5 offense gems?
!opt_gems
tier: 5

Maybe you switched a weapon and took off your tier 5 and 6 offense gems.
!opt_gems
amount: 8
tier: 10
offset: offense_gem: -6, offense_gem: -5, wep_atk: 1000, wep_rarity: legendary
"""

help_rant = \
"""
Rants and Waahhhnts. Mostly sarcastic, but there is always a hint of truth.
"""

help_remove = \
"""
Removes your stats. You will need to `!set` your stats again later.
"""

help_set = \
"""
Set the stats for your character.
You can get the stats template using the `!template` command, then run the following:

!set
job: wizard
fatal_strikes: 0

str: 17
dex: 18
...

You can also update individual values instead of entering all the attributes:

!set
boss_dmg: 6.9
pet_tier: epic
kandura_pendant: true
"""

help_stats = \
"""
View character's current stats. Additional info includes:

Crit Chance: Chance to land critical hit. `!help crit_chance` for more details.
Dmg Tot (Additive or Multiplicative): Calculation settings for `dmg_tot`.
Enemy: Based on the `vs` setting in stats. `!help mob` for more details.
Gear Effect(s): List of all actives from gears used in calculations, ie. Kandura's Pendant, Murpagoth.
Multiplier: Rating of stats. `!help multiplier` for more details.
Pet Bonus Atk: Amount of bonus attack gained from pet.
Wep Rarity: Weapon rarity.

To view your own stats, simply invoke `!stats` (or `!stats @YOUR_NAME`)
To view your friend's stats, invoke `!stats @MY_FRIEND`
"""

help_template = \
"""
Send a stats template. Some detail on some of the attributes.

job: One of: {}.
fatal_strikes: Value is skill level. Set as 0 to exclude from calculations.

luk: Primary stat for Assassin, Thief. Increases critical hit chance for all classes.

wep_rarity: Rarity affects multiplier on bonus attack. One of: {}.
wep_effect: Weapon effect. `!info wep_effect` for more detail.

<NON_WEP_EFFECT>: One of: true, t, yes, y, false, f, no, n. Calculations consider only perfect effect uptime. <NON_WEP_EFFECT> is one of: {}.
Example:
kandura_pendant: False. Kandura's Pendant's 3% attack buff will not be used in calculations.

b_atk: Bonus attack. Pet bonus is not included here but is used in calculations.
pm_atk: Physical or magic attack. Main stats are converted to these.
piercing: Piercing. Hardcapped to 30%.
pm_piercing: Physical or magic piercing. Softcapped at 16.6% as of 2018 December.
crit_rate: Critical rate. This is NOT the critical hit chance.
crit_dmg: Critical damage. 10 critical damage is 1% increase in critical damage.
mr_dmg: Melee or ranged damage.
ele_dmg: Elemental damage. Consider your uptime and only relevant elements (if you have 20% fire damage but use fire skills only half the time, enter 10%).
 
vs: Versus a specific mob. `!help mob` for more details.
dmg_tot_add: One of: true, t, yes, y, false, f, no, n. Affects the behaviour of `dmg_tot` in calculations. Read the `NOTE` below.

NOTE: `mr_dmg`, `ele_dmg`, `boss_dmg` are summed into a single multiplier called `PDM` (percentage damage multiplier). They're essentially all equivalent. `dmg_tot` is either summed or multiplied by `PDM` (unknown because no client damage formula). If you have any information regarding this, please let me know!
""".format(util.job_ids(), util.rarity_ids(), util.non_wep_effect_ids())

info_cape = \
"""
Make sure to read about diminishing returns using `!info dim_ret` first.

Let's consider perfect uptime, which may be unrealistic depending on the enemy you're fighting.

Varrekant's Wings give a 10.0% piercing boost for 8s with a 24s internal cooldown. People say it is equivalent to a constant 10.0 * 8 / 24 = 3.3% piercing, but it is actually slightly better than 3.3% due to piercing's exponential property.

The math is:
100 / (100 - min(30, PIERCING + 10)) * UPTIME + 100 / (100 - min(30, PIERCING)) * (1 - UPTIME)
where PIERCING is your base piercing without Wings and UPTIME = 8 / 24.

If you have 0% base piercing, constant 3.3% piercing is a 3.4% damage increase while Varrekant's Wings is an overall 3.7% increase in damage.
At 20% piercing, constant 3.3% piercing is a 4.3% increase while Varrekant's Wings is a 4.8% increase in damage.

This means an Absolute Cape (or Balrog Wings) with decent piercing and secondary attribute (or 3.5% piercing with a useless secondary roll) can compete or be better than Varrekant's Wings. This is even more true under realistic scenarios (our calculations are based on PERFECT execution). A perfect Cape is better than Varrekant's Wings for most classes. The argument gets muddy if we consider the additional stats from Varrekant's Wings - attack speed is an offense stat for certain classes such as Heavy Gunners or a convenience stat for others such as Wizards (never cancel your 'nados with TPs again!).

If Varrekant's Wings overcap your piercing by a good amount, consider switching to an Absolute Cape or Balrog Wings since piercing above 30.0% is completely useless.
"""

info_compare_key = \
"""
The following are the keys used in `!compare`.

job, fatal_strikes
str, dex, int, luk
pet_level, pet_rarity

kandura_pendant, katvan_horns, varrekant_horns, varrekant_wings

wep_rarity, wep_atk, wep_effect
`wep_effect` takes a weapon effect value. `!info wep_effect` for more details.

b_atk, pm_atk, piercing, pm_piercing, crit_rate, crit_dmg, mr_dmg, ele_dmg, boss_dmg, dmg_tot

vs, dmg_tot_add

main_stat_gem, offense_gem
"""

info_credit = \
"""
Thank you to:

- Me (Refraction on NA West): getting some data, referencing against various sources, writing the bad code.
- u/gaffox for the damage calculator on Reddit (https://www.reddit.com/r/MapleStory2/comments/a1nj5j/damage_calculator/).
- u/Rhygrass for the gearing guide (https://www.reddit.com/comments/9x2kff).
- u/TrylessDoer for the critical chance formula on Reddit (https://www.reddit.com/r/MapleStory2/comments/a36vrq/datamined_critical_rate_algorithm/) and the GitHub project on the Comparison Calculator under TimeBomb (https://github.com/TimeBomb/ms2-dmg-calc).
- sztupy for the Lua 5.1 decompiler on Github (https://github.com/sztupy/luadec51).
- Meowuniverse for formulas and data (https://www.meowuniverse.com/stats).
- Miyuyami for the unpacker on Github (https://github.com/Miyuyami/MS2Tools).
- Wunkolo for the unpacker on Github (https://github.com/Wunkolo/Maple2Tools).

Feel free to edit this Bot however you wish, but please credit the above people.
"""

info_dim_ret = \
"""
The current damage formula sums boss/melee/ranged/elemental damage together. Total damage is added or multiplied depending on stats setting `dmg_tot_add`. Let's refer to this sum as percent damage multiplier `PDM`.

Let's say you have no PDM. So your original PDM is 1.00.
You find a 4% boss damage gear. Your new PDM is 1.00 + 0.04 = 1.04.
You are now 1.04 / 1.00 = 1.04 times stronger - a 4% increase in overall damage.

Let's say you are a Wizard with 40% PDM thanks to the elemental passives.
You find a 4% boss damage gear. Your new PDM is 1.40 + 0.04 = 1.44.
You are now 1.44 / 1.40 = 1.029 - a 2.9% increase in overall damage.

PDM stats and most other stats such as main stat, bonus attack, and critical damage are uncapped, but they immediately suffer from diminishing returns. The initial damage increase is 1%, dropping to 0% at infinity. There is a 'breakpoint' between 1% and 0% that some people are referring to, and they say that you will need to start finding physical or magic attack on gears after. They're not wrong, but for jobs with high attack and low PDM (most physical attack jobs), hitting the breakpoint is hard. For jobs with low attack and high PDM (Wizards), hitting the breakpoint is much easier, so only these classes have to worry about the breakpoints.

Physical or magic piercing has an initial damage increase of 1.2% and drops to 1.0% at the softcap.

Piercing is the exception since it's the only multiplier with the changing factor on the denominator. The initial damage increase is 1.0% and exponentially increases to 1.4% at the hardcap.
"""

info_gem = \
"""
Make sure to read about diminishing returns using `!info dim_ret` first.

At endgame, most players should have one high tier accuracy gem (maybe even two later). The rest are spread between offense and main stat gems.
There is a `weapon` and a `physical/magic attack` multiplier. Bonus attack (offense gems and pet) contributes to weapon while main stat gems contribute to p/m attack. Like most stats, both have diminishing returns.
On top of diminishing returns, bonus attack is less effective with more weapon attack (also why Epic pets scale off), but are strong currently since most weapons are Epic. Note that bonus attack is multiplied with a coefficient determined by the weapon rarity. Legendary weapons have around twice that of an Epic (else bonus attack falls off hard).

Main stat is converted to p/m attack and only suffers from diminishing returns. There is a breakpoint between bonus attack and main stat as you get higher weapon attack.
At endgame, a mix of offense and main stat gems may be needed. As you get more weapon attack, focus is shifted to main stat gems. Use the `!compare` to experiment.

Upgrading tips:
Always be upgrading one accuracy gem to as high as possible. Get it to T10 before working on another.
Upgrade offense gems evenly (across all gems) because they gain less bonus attack and have lower enchant chances at higher tiers. The general consensus is to cap the offense gems at tier 5, since tier 6 has decreased bonus attack gain. The focus should shift toward upgrading main stat gems later.
Upgrade main stat gems a few at a time because they gain more main stat at higher tiers and barely decrease in success chance (mathematically, should not focus on just one).
"""

info_limitation = \
"""
Current code stores character stats in a dictionary and caches it to disk on every `!set`. Clearly not the way to handle concurrency and memory, but that's not the purpose of this project. `storage.py` can be refactored to use a database instead to fix this.
Expect lots of bugs as I made this to be flexible yet have no tests whatsoever. Most normal usages should work, though.

Formula and data in the calculations are the result of assumptions, datamining (values may differ by region), tests by others on various different servers, and referencing against those mentioned in `!info credit`. Currently, I do not fully know if `damage total percent` is additive or multiplicative with other percentage-based damages or if Kandura's 3% physical/magic attack increase is additive or multiplicative with Varrekant's Horns' active effect. For now, `damage total percent` is multiplicative but can be set to additive by setting `dmg_tot_add` while everything else is hard-coded to be additive.

Currently, my main is a Wizard. I've also played Priest and Thief before KMS2 Restart patch and Assassin in GMS2, and there are still many jobs that I haven't played. There may be jobs with skills that boost certain stats but do not update the stats page. Pet bonus attack, for example (I know it's not a skill), does not show up on the stats page. Currently, I've only take into account Assassin's Fatal Strikes skill. If I am missing a skill or anything else with hidden stat boosts, please let me know!

Accuracy is not considered since I do not know the formula.

Only offensive stats are considered currently. Attack speed, a great stat comparable to boss damage for certain jobs such as Heavy Gunners, is not considered due to the sheer complexity involved. Skill animation speeds and attack speed formulas seem to be in the client at a quick glance, but there are just too many factors to consider so I will not be including it.
"""

info_neck = \
"""
Make sure to read about diminishing returns using `!info dim_ret` first.

Kandura's Pendant is a great temporary item for some easy accuracy. The 3% increase to physical or magic attack is equivalent to an increase in overall damage based on current formulas (when stacked with Varrekant's Horns, this may have diminishing returns). However, I believe it is not BiS in MS2 once you have a high level accuracy gem and a decent Absolute Pendant (it might've been the best if we had Orange % gems, but we don't).

Let's first look at some RNG and economics.
Minimum number of Pendants for a 3-socket is eight.
The expectation for a 3-socket is (1 / 0.50 + 1) * 1 / 0.45 * 1 / 0.40 = 17 Pendants.
The distribution lies to the right of 17 Pendants since the minimum number is set at 8, yet you can fail an infinite number of Pendants.
Let's assume most people would need 30 Pendants. That's a lot of Pendants (30-35m per Pendant on NAW in December 2018).

Let's now compare a 3-socket Kandura's with a 3-socket Absolute Necklace on my hypothetical 'endgame' Wizard (26% piercing, 45% PDM, tier 10 main stat gems). We use two main stat gems for Necklace (third for acuracy) and three for Kandura's.

!compare
kandura_pendant: true, main_stat_gem: 10, main_stat_gem: 10, main_stat_gem: 10
piercing: 3.0, boss_dmg: 3.0, main_stat_gem: 10, main_stat_gem: 10
piercing: 4.0, boss_dmg: 6.0, main_stat_gem: 10, main_stat_gem: 10

Relative to my base, Kandura's is 25.1%, imperfect Necklace is 24.1%, and perfect Necklace is 28.3%.
Relative to each other, Kandura's is 0.8% stronger than the imperfect Necklace. The perfect Necklace is 3.3% stronger than Kandura's.
The numbers are fairly close, but Kandura's requires significantly more investment. Play around with `!compare` to test this on your character.

TL;DR
3-socket Kandura's requires ~30 Pendants, is close in performance to a decent Absolute Necklace, and is worse than a perfect Absolute Necklace.
"""

info_wep = \
"""
To double pierce or not to double pierce, that is the question.

Epic Weapon:
For the longest time, I had used a piercing and total damage Epic staff on my Wizard, capping my magic resist at 16.6% through gloves, pet, necklace, and guild buff. However, transitioning into a piercing/boss damage necklace, poorly rolled epic pet, and low magic piercing gloves meant sitting around 5% total magic piercing after buffs. That is a lot of missing magic piercing, arguably the second best stat in the game before the softcap. I would definitely recommend new players to go for a double piercing Epic weapon to avoid the headache of capping physical or magic piercing. Remember that the Epic weapon is a temporary gear that should facilitate you in acquiring a legendary weapon and is not the be all end all.

Legendary weapon:
This is a bit more controversial. For the current content (December 2018), double piercing is the most practical. However, this may change with the release of Infernog. The current NA Infernog file in the client has Infernog at 150 resist (10% physical/magic piercing), the same as KR/CN. If it remains this way, piercing with damage total or physical/magic attack would become the best combinations. However, if Nexon pulls a fast one and buffs Infernog's resist (CDev/CMoc/CPap had buffed HP but the resist remained the same), double piercing may remain the most practical if not the best.

GMS2 is different from KMS2 and CMS2 in that we may be getting Infernog way before CRog, CVar, Madria, etc. The other regions can afford to get rid of double piercing because their endgame gradually gets less resist (culminating in Infernog until recently) while our endgame may eventually be Madria who has a resist of 225 (15% physical/magic piercing) if we follow KR/CN. Depending on what Nexon NA decides to change, I would suggest keeping both a copy of double piercing and piercing + damage total/atk legendary weapon.
"""

info_wep_effect = \
"""
The active effect from the weapon. The following are the effects used in calculations:
{}
""".format(util.wep_effect_ids())

template = """
Please read `!help template` first. Copy the following into a text editor and update the values. Invoke the `!set` command after.

!set
# Lines starting with '#' are comments and won't be executed.
job: wizard
fatal_strikes: 10

str: 0
dex: 0
int: 0
luk: 0

wep_rarity:  epic
wep_atk:     0
# `!info wep_effect` for all effects
wep_effect:  murp

kandura_pendant: false
varrekant_wings: false
varrekant_horns: false
katvan_horns:    false

pet_rarity:  exceptional
pet_level:   0

b_atk:       0
pm_atk:      0
piercing:    0
pm_piercing: 0
crit_rate:   0
crit_dmg:    100
mr_dmg:      0
ele_dmg:     0
boss_dmg:    0
dmg_tot:     0

# `!help mob` for all enemies
vs: cdev
dmg_tot_add: false
"""
