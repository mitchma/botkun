from datetime import datetime
from src.mob import mob
import src.data as data

should_log = True

def log(message):
    if not should_log:
        return
    time = datetime.now()
    print("{}-{:02d}-{:02d} {:02d}:{:02d}:{:02d} - {}".format(time.year,
          time.month, time.day, time.hour, time.minute, time.second, message))

def toggle_log(log_state = None):
    global should_log
    if log_state is None:
        should_log = not should_log
    else:
        should_log = log_state

def disc_id(disc_id):
    if not is_disc_id(disc_id):
        log("'{}' is an invalid Discord user".format(disc_id))
        msg = "The given Discord user is invalid".format(disc_id)
        raise Exception(msg)
    disc_id = disc_id[2:-1]
    disc_id = ''.join(c for c in disc_id if c.isdigit())
    return disc_id

def is_disc_id(disc_id):
    good_prefix = disc_id.find("<@") == 0 and disc_id.count("<@") == 1
    good_suffix = disc_id[-1] == '>' and disc_id.count('>') == 1
    return good_prefix and good_suffix

def mob_ids():
    return ', '.join(mob.keys())

def job_ids():
    return ', '.join(data.job.keys())

def rarity_ids():
    return ', '.join(data.rarity.keys())

def gear_effect_id():
    return ', '.join(data.stats_template["gear_effect"].keys())

def wep_effect_ids():
    return ', '.join(data.wep_effect.keys())

def non_wep_effect_ids():
    return ', '.join(data.non_wep_effect.keys())
