import json
import os
import src.local_storage as local_storage
import src.database as database

from src.util import log

is_database = False

def init():
    if is_database:
        database.init()
    else:
        local_storage.restore()

# Toggle between local storage and database
def toggle(storage_state = None):
    global is_database
    if storage_state is None:
        is_database = not is_database
    else:
        is_database = storage_state

    log("storage::toggle(): Storage mode is now '{}'"
        .format("database" if is_database else "local storage"))

def get(stats_id):
    # log("storage::get(): Retrieving character stats with ID '{}'"
    #     .format(stats_id))
    stats = database.get(stats_id) if is_database \
            else local_storage.get(stats_id)
    return stats

def set(stats_id, stats):
    # log("storage::set(): Setting character stats with ID '{}'".format(stats_id))
    if is_database:
        database.set(stats_id, stats)
    else:
        local_storage.set(stats_id, stats)
        local_storage.cache()

def remove(stats_id):
    # log("storage::remove(): Removing character stats with ID '{}'"
    #     .format(stats_id))
    if is_database:
        database.remove(stats_id)
    else:
        local_storage.remove(stats_id)
        local_storage.cache()

# Local storage only
def restore():
    if not is_database:
        log("storage::restore(): Restoring from local storage")
        local_storage.restore()
    else:
        log("storage::restore(): Database in use, no local storage needed")

def cache():
    """
    Very naive implementation that does not handle race conditions.
    Local storage only.
    """
    if not is_database:
        log("storage::cache(): Caching to local storage")
        local_storage.cache(stats_id)
    else:
        log("storage::restore(): Database in use, no local storage needed")
