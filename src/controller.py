import random
import src.data as data
import src.formula as formula
import src.input_parser as input_parser
import src.program_data as program_data
import src.storage as storage

from src.mob import mob
from src.rants import rants
from src.util import log
from copy import deepcopy

def get(stats_id):
    """
    stats_id (str)
    """
    # log("controller::get(): Retrieving character stats with ID '{}'"
    #     .format(stats_id))
    stats = storage.get(stats_id)
    return stats

def set(stats_id, inputs):
    """
    stats_id (str)
    inputs (str)
    """
    # log("controller::set(): Setting character stats with ID '{}'"
    #     .format(stats_id))
    # try getting existing stats
    try:
        existing_stats = storage.get(stats_id)
    # character stats does not exist currently
    except:
        existing_stats = None

    # create new stats or update existing
    try:
        stats = input_parser.parse_stats(inputs, stats = existing_stats)
    except Exception as e:
        log("controller::set(): Error setting character stats with ID '{}'\n{}"
            .format(stats_id, e))
        raise e
    storage.set(stats_id, stats)
    return stats

def remove(stats_id):
    """
    stats_id (str)
    """
    # log("controller::remove(): Removing character stats with ID '{}'"
    #     .format(stats_id))
    try:
        storage.remove(stats_id)
    except Exception as e:
        raise e

def compare(stats_id, inputs):
    """
    Take a stats_id whose stats will serve as the basis for stat combinations
    stored in 'inputs'. Produce a list of dictionaries with information on the
    given stat combination, the percentage change in damage, and the multiplier.

    stats_id (str): Character stat ID whose stats will serve as the basis.
    inputs (str): Stats to add to the base character stats
    return (list): A list of dictionaries with the stat combination, multiplier,
                   and damage % changes.
    """
    # log("controller::compare(): Comparing stats for ID '{}'".format(stats_id))
    stats = storage.get(stats_id)
    comparisons = input_parser.parse_compare(stats, inputs)
    base_multiplier = formula.final_mult(stats)
    results = []
    for stats in comparisons:
        new_multiplier = formula.final_mult(stats["stats"])
        dmg_change = (new_multiplier / base_multiplier - 1) * 100 \
                     if base_multiplier != 0 else 0.0
        results.append({
            "combination": stats["combination"],
            "dmg_change": dmg_change,
            "multiplier": new_multiplier,
        })
    return results

def crit_chance(stats_id):
    """
    stats_id (str)
    """
    # log("controller::crit_chance(): Calculating critical hit chance for ID '{}'"
    #     .format(stats_id))
    stats = get(stats_id)
    return formula.crit_chance(stats)

def final_mult(stats_id):
    """
    stats_id (str)
    """
    log("controller::final_mult(): Calculating multiplier for ID '{}'"
        .format(stats_id))
    stats = get(stats_id)
    return formula.final_mult(stats)

def mob_info(mob_id):
    """
    mob_id (str)
    """
    # log("controller::mob(): Retrieving information on mob with ID '{}'"
    #     .format(mob_id))
    if mob_id not in mob:
        msg = "No mob with ID '{}' exists".format(mob_id)
        raise Exception(msg)
    mob_dict = deepcopy(mob[mob_id])
    mob_dict["id"] = mob_id
    return mob_dict

def multiplier(stats_id):
    """
    stats_id (str)
    """
    # log("controller::multiplier(): Retrieving character stats with ID '{}'"
    #     .format(stats_id))
    stats = storage.get(stats_id)
    return formula.final_mult(stats)

def optimize_attributes(stats_id, inputs):
    """
    stats_id (str)
    inputs (str)
    """
    # log("controller::optimize_attributes(): Optimizing attribute points for ID "
    #     "'{}'".format(stats_id))
    attr_pts = input_parser.parse_attr_pts(inputs)
    stats = deepcopy(get(stats_id))
    main_stat = data.job[stats["job"]]["main_stat"]
    base_ms = stats["stats"][main_stat]
    base_cr = stats["attr"]["crit_rate"]
    base_pm_atk = stats["attr"]["pm_atk"]

    max_mult = 0.0
    opt_ms_pts = 0
    opt_crit_pts = 0

    for i in range(attr_pts + 1):
        ms_pts = i
        crit_pts = attr_pts - i

        stats["stats"][main_stat] = base_ms + ms_pts
        stats["attr"]["crit_rate"] = base_cr + crit_pts * 3
        stats["attr"]["pm_atk"] = base_pm_atk + ms_pts *\
                                  data.job[stats["job"]]["main_stat_to_pm_atk"]
        curr_mult = formula.final_mult(stats)
        if curr_mult > max_mult:
            max_mult = curr_mult
            opt_ms_pts = ms_pts
            opt_crit_pts = crit_pts

    optimal_attributes = {
        "crit_rate_pts": opt_crit_pts,
        "main_stat": main_stat,
        "ms_pts": opt_ms_pts,
        "multiplier": max_mult,
    }
    return optimal_attributes

def optimize_gems(stats_id, inputs):
    """
    Take a stats_id whose stats will serve as the basis.

    stats_id (str): Character stat ID whose stats will serve as the basis.
    inputs (str): Stats to add to the base character stats
    return (list): A list of dictionaries with the stat combination, multiplier,
                   and damage % changes.
    """
    # log("controller::compare(): Comparing stats for ID '{}'".format(stats_id))
    stats = storage.get(stats_id)
    comparisons = input_parser.parse_gems(stats, inputs)
    base_multiplier = formula.final_mult(stats)
    results = []
    for stats in comparisons:
        new_multiplier = formula.final_mult(stats["stats"])
        dmg_change = (new_multiplier / base_multiplier - 1) * 100 \
                     if base_multiplier != 0 else 0.0
        results.append({
            "combination": stats["combination"],
            "dmg_change": dmg_change,
            "multiplier": new_multiplier,
        })
    return results

def rant():
    # log("controller::rant(): Ranting")
    rand_rant = random.choice(rants)
    return rand_rant

def stats(stats_id):
    """
    stats_id (str)
    """
    # log("controller::stats(): Retrieving character stats with ID '{}'"
    #     .format(stats_id))
    stats = get(stats_id)
    return stats

def template():
    # log("controller::template(): Retrieving character stats template")
    return program_data.template
