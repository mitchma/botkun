import src.data as data
import src.storage as storage
import src.util as util

from copy import deepcopy
from src.mob import mob
from src.util import log

def parse_gems(stats, inputs):
    """
    Take a base stats and a set of stat combinations stored in 'inputs' and
    produce a list of dictionaries containing the stat combinations and the
    resulting character stats. The first item in the list is the base stats.

    stats (dict): Base character stats.
    inputs (str): Number of gems to slot and gems to remove.
    return (list): A list of character stats.
    """
    inputs = inputs.lower().replace('=', ':')
    inputs = inputs.split('\n')
    num_gems, tier, offset_stats = 9, 10, ""
    for line in inputs:
        if ':' not in line:
            msg = "Please make sure to provide the parameters with the " \
                  "following format:\nKey: Value\nRefer to `!help opt_gems` " \
                  "for more details"
            raise Exception(msg)
        key, val = line.split(':', 1)
        key, val = key.strip(), val.strip()
        if key == "amount":
            try:
                num_gems = int(val)
                assert 0 <= num_gems <= 9
            except:
                msg = "The number of gems must be between 0 and 9, not '{}'" \
                      .format(val)
                raise Exception(msg)
        elif key == "tier":
            try:
                tier = int(val)
                assert -10 <= num_gems <= 10
            except:
                msg = "The number of gems must be between -10 and 10, not " \
                      "'{}'".format(val)
                raise Exception(msg)
        elif key == "offset":
            offset_stats = val

    # test if offset_stats is correct
    parse_compare(stats, offset_stats)

    compare_str = ""
    for i in range(0, num_gems + 1):
        gem_str = [ "main_stat_gem: {}".format(tier) ] * i \
                  + [ "offense_gem: {}".format(tier) ] * (num_gems - i)
        gem_str = ', '.join(gem_str)
        compare_line = "{}, {}\n".format(offset_stats, gem_str)
        compare_str += compare_line
    comparisons = parse_compare(stats, compare_str)

    for i, comparison in enumerate(comparisons[1:]):
        comparison["combination"] = {
            "main_stat_gem": i,
            "offense_gem": num_gems - i,
            "tier": tier,
        }

    return comparisons

def parse_stats(inputs, stats = None, acc = False):
    """
    Take in a set of stats and either sets or adds them onto the given character
    stats template or a default template.

    inputs (str): Stats to set/add for character stats.
    stats (dict): Base stats to modify. A default template is used if 'None'.
    acc (bool): If true, accumulate numerical stats other than 'skill' level and
                'pet level', else set as absolute value.
    return (dict): Character stats.
    """

    # get stats name and value
    def _parse_attr(attr):
        try:
            key, val = attr.split(':')
        except Exception as e:
            log(e)
            msg = "Parsing error:\n{}".format(attr)
            msg += "\nThe format should be 'compare_key: value'"
            raise Exception(msg)
        return key.strip(), val.strip()

    # cast given value
    def _cast_val(line, key, val):
        try:
            func = data.val_type[key]
            if func == bool:
                if val in ("true", "t", "yes", "y"):
                    val = True
                elif val in ("false", "f", "no", "n"):
                    val = False
                else:
                    raise
            elif func == int:
                val = int(round(float(val)))
            else:
                val = func(val)
        except Exception as e:
            log(e)
            raise Exception("Parsing error:\n{}\n'{}' has the wrong value "
                            "type in '{}'".format(line, key, val))
        return val

    # set all headgear effects to false
    def _remove_head_effects(stats):
        for key in ("katvan_horns", "varrekant_horns"):
            stats["gear_effect"][key] = False

    # set all weapon effects to false
    def _remove_wep_effects(stats):
        for key in data.wep_effect:
            stats["gear_effect"][key] = False

    # log("input_parser::parse_stats()")
    inputs = inputs.lower().replace('=', ':')

    # create new character stats
    if stats is None:
        stats = deepcopy(data.stats_template)

    for line in inputs.split('\n'):
        # ignore empty or comment lines
        line = line.strip()
        if line == "" or line[0] == '#':
            continue

        key, val = _parse_attr(line)

        if key == "job":
            if val not in data.job:
                msg = "Parsing error:\n{}\n'{}' is not a job. The values are:" \
                      "\n{}\n".format(line, val, util.job_ids())
                raise Exception(msg)
            stats[key] = val
        elif key == "wep_effect" or key in data.stats_template["gear_effect"]:
            # convert wep_effect value to wep_effect name (murp, panic_double)
            if key == "wep_effect":
                if val not in data.wep_effect:
                    msg = "Parsing error:\n{}\n'{}' is not a weapon effect. " \
                          "The values are listed in `!info wep_effect`" \
                          .format(line, val)
                    raise Exception(msg)
                sec_key, val = val, "true"
            else:
                sec_key = key

            val = _cast_val(line, sec_key, val)
            if stats["gear_effect"][sec_key] != val:
                # only one active weapon effect
                if key == "wep_effect":
                    _remove_wep_effects(stats)
                # only one active headgear effect
                elif key in ("katvan_horns", "varrekant_horns"):
                    _remove_head_effects(stats)
            stats["gear_effect"][sec_key] = val
        elif key in data.stats_template["attr"] \
                or key in data.skill \
                or key in data.stats_template["stats"] \
                or key in data.stats_template["option"] \
                or key in ("pet_level", "pet_rarity", "wep_rarity", "wep_atk",
                	       "vs", "dmg_tot_add"):
            if key in data.stats_template["attr"]:
                pri_key, sec_key = "attr", key
            elif key in data.skill:
                pri_key, sec_key = "skill", key
            elif key in data.stats_template["stats"]:
                pri_key, sec_key = "stats", key
            elif key in data.stats_template["option"]:
                pri_key, sec_key = "option", key
            elif key in ("pet_level", "pet_rarity"):
                pri_key, sec_key = "pet", key.replace("pet_", "")
            else: # if key in ("wep_atk", "wep_rarity"):
                pri_key, sec_key = "wep", key.replace("wep_", "")
            val = _cast_val(line, key, val)

            if sec_key == "rarity" and val not in data.rarity:
                msg = "Parsing error:\n{}\n'{}' is not a rarity. The values " \
                      " are:\n{}\n".format(line, val, util.rarity_ids())
                raise Exception(msg)
            elif sec_key == "vs" and val not in mob:
                msg = "Parsing error:\n{}\n'{}' is not a mob. The values " \
                      "are:\n{}\n".format(line, val, util.mob_ids())
                raise Exception(msg)

            # set or accumulate stats
            if acc:
                if pri_key in ("stats", "attr"):
                    if sec_key == "crit_dmg":
                        val /= 10.0
                    stats[pri_key][sec_key] += val
                    if sec_key == data.job[stats["job"]]["main_stat"]:
                        stats["attr"]["pm_atk"] += val \
                            * data.job[stats["job"]]["main_stat_to_pm_atk"]
                elif key == "wep_atk":
                    stats["wep"]["atk"] += val
                # skill level and pet level takes absolute values
                else:
                    stats[pri_key][sec_key] = val
            else:
                stats[pri_key][sec_key] = val
        # only for accumulation
        elif key in data.gemstone and acc:
            if key == "acc_gem":
                pri_key, sec_key = "attr", "acc"
            elif key == "main_stat_gem":
                pri_key, sec_key = "stats", data.job[stats["job"]]["main_stat"]
            else:
                pri_key, sec_key = "attr", "b_atk"
            val = _cast_val(line, key, val)

            try:
                stats[pri_key][sec_key] += data.gemstone[key][val]
            except Exception as e:
                log(e)
                msg = "Parsing error:\n{}\n'{}' must be a tier level from " \
                      "-10 to 10 and not '{}'".format(line, key, val)
                raise Exception(msg)
            # main stats converted to physical/magic attack
            if key == "main_stat_gem":
                stats["attr"]["pm_atk"] +=  data.gemstone[key][val] \
                    * data.job[stats["job"]]["main_stat_to_pm_atk"]
        elif key in data.stats_template["stats"]:
            assert(acc)
        else:
            msg = "Parsing error:\n{}\n'{}' is not a key".format(line, key)
            raise Exception(msg)
    return stats

def parse_compare(stats, inputs):
    """
    Take a base stats and a set of stat combinations stored in 'inputs' and
    produce a list of dictionaries containing the stat combinations and the
    resulting character stats. The first item in the list is the base stats.

    stats (dict): Base character stats.
    inputs (str): Stats to add to the base character stats
    return (list): A list of character stats.
    """

    def _parse_combination(attrs):
        """
        Converts the stat combinations to a command for parse_stats().

        attrs (str): A combinations of attributes.
        return (str, dict): String Commands and base stats for parse_stats().
        """
        inputs = ""
        base_stats = None

        for attr in attrs.split(","):
            attr = attr.strip()
            if attr == "":
                continue
            elif util.is_disc_id(attr):
                base_stats = storage.get(util.disc_id(attr))
            elif attr.count(':') != 1:
                msg = "Parsing error:\n{}".format(attr)
                msg += "\nThe format should be 'compare_key: value' or " \
                       " '@MyFriend'"
                raise Exception(msg)
            else:
                inputs += "{}\n".format(attr)
        return inputs, base_stats

    # log("input_parser::parse_compare()")
    inputs = inputs.lower().replace('=', ':')
    char_stats = [{
        "combination": "base",
        "stats": deepcopy(stats),
    }]

    # convert each line to a parse_stats suitable input and get new stats
    for line in inputs.split('\n'):
        # ignore empty or comment lines
        line = line.strip()
        if line == "" or line[0] == '#':
            continue
        try:
            inputs, base_stats = _parse_combination(line)
            if base_stats is None:
                base_stats = stats
            base_stats = deepcopy(base_stats)
            new_stats = parse_stats(inputs, stats = base_stats, acc = True)
        except Exception as e:
            log(e)
            msg = "{}\n\nThe offending line is:\n{}".format(e, line)
            if "Character stats with ID" in msg:
                msg += "\nMake sure that everyone's character stats have " \
                       "been `!set`."
            raise Exception(msg)
        char_stats.append({
            "combination": line,
            "stats": new_stats,
        })
    return char_stats

def parse_attr_pts(inputs):
    """
    inputs (str): Attribute points.
    return (int)
    """
    # log("input_parser::parse_attr_pts()")
    try:
        attr_pts = int(inputs)
    except Exception as e:
        log(e)
        msg = "Parsing error:\nPlease enter the number of attribute points " \
              "to allocate and not '{}'".format(inputs)
        raise Exception(msg)
    return attr_pts
