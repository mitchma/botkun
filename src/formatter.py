import src.formula as formula

def codify(string):
    """
    string (str)
    """
    msg = "```\n{}\n```".format(string)
    return msg

def discord_wrap(string, is_error = False, mentions = None):
    """
    string (Error or str)
    is_error (bool)
    """
    string = str(string)
    if mentions:
        for user in mentions:
            string = string.replace(user.id, user.display_name)
    msg = "Oh no, something went wrong!\n\n{}".format(string) if is_error \
          else string
    msg = codify(msg)
    return msg

def format_stats(stats):
    """
    stats (dict)
    """
    stats_string = stats["job"].replace('_', ' ').title()
    if stats["job"] == "assassin":
        stats_string += "\nFatal Strikes level: {:>2}" \
                       .format(stats["skill"]["fatal_strikes"])
    tmp = stats["stats"]
    
    stats_string += """\n
Str    Dex    Int    Luk
{:>3}    {:>3}    {:>3}    {:>3}
    """.format(tmp["str"], tmp["dex"], tmp["int"], tmp["luk"])

    tmp = stats["pet"]
    pet_b_atk = int(round(formula.pet_b_atk(stats)))
    stats_string += """
Pet    Level    Bonus Atk    Rarity
       {:<2}       {:<3}          {}
    """.format(tmp["level"], pet_b_atk, tmp["rarity"].capitalize())
    stats_string += """
Enemy     Wep Rarity     Multiplier
{:<5}     {:<11}    {:.1f}
    """.format(stats["option"]["vs"].title(), stats["wep"]["rarity"].title(),
               round(formula.final_mult(stats), 1))
    gear_effect = "\nGear Effect(s):\n"
    effects = []
    for effect in stats["gear_effect"]:
        if stats["gear_effect"][effect]:
            effects.append(effect.replace('_', ' ').title())
    if effects:
        stats_string += gear_effect + ', '.join(effects)

    tmp = stats["attr"]
    crit_chance = formula.crit_chance(stats) * 100
    mode = "Additive" if stats["option"]["dmg_tot_add"] else "Multiplicative"
    stats_string += """

Wep Atk      {:>8}
Bonus Atk    {:>8}
P/M Atk      {:>8}
Piercing     {:>8.1f} %
P/M Piercing {:>8.1f} %
Crit Rate    {:>8}
Crit Dmg     {:>8.1f} %
Crit Chance  {:>8.1f} %
M/R Dmg      {:>8.1f} %
Ele Dmg      {:>8.1f} %
Boss Dmg     {:>8.1f} %
Dmg Tot      {:>8.1f} % ({})""" \
    .format(stats["wep"]["atk"], tmp["b_atk"], tmp["pm_atk"], tmp["piercing"],
            tmp["pm_piercing"], tmp["crit_rate"], tmp["crit_dmg"], crit_chance,
            tmp["mr_dmg"], tmp["ele_dmg"], tmp["boss_dmg"], tmp["dmg_tot"],
            mode)

    return stats_string

def format_compare(comparisons):
    """
    comparisons (dict)
    """
    # base stats
    cmp_string = "This is your base stats\nDmg % Diff: {:>12.1f} %\n" \
                 "Multiplier: {:>12.1f}\n".format(comparisons[0]["dmg_change"],
                                                  comparisons[0]["multiplier"])

    # given stat combinations
    for i, info in enumerate(comparisons[1:], 1):
        cmp_string += "\nCombination {}\n{}\nDmg % Diff: {:>12.1f} %\n" \
                      "Multiplier: {:>12.1f}\n".format(i, info["combination"],
                                                       info["dmg_change"],
                                                       info["multiplier"])
    return cmp_string

def format_optimal_attributes(opt_attr):
    """
    opt_attr (dict)
    """
    opt_attr_string = "Put {} point(s) into '{}' and {} point(s) into " \
                      "'Critical Rate' to get a new multiplier of {:.1f}" \
                      .format(opt_attr["ms_pts"], opt_attr["main_stat"].title(),
                              opt_attr["crit_rate_pts"], opt_attr["multiplier"])
    return opt_attr_string

def format_optimal_gems(comparisons):
    """
    comparisons (dict)
    """
    # base stats
    cmp_string = "Base multiplier: {:.1f}".format(comparisons[0]["multiplier"])

    cmp_string += "\n\nM  O  Dmg % Diff    Multiplier\n"
    data_row = "{:<3}{:<3}{:>8.1f} %{:>14.1f}\n"

    # given stat combinations
    for info in comparisons[1:]:
        cmp_string += data_row.format(info["combination"]["main_stat_gem"],
                                      info["combination"]["offense_gem"],
                                      info["dmg_change"],
                                      info["multiplier"])
    tier = 10 if len(comparisons) <= 1 \
           else comparisons[1]["combination"]["tier"]
    cmp_string += "\nM = T{0} Main Stat Gem\nO = T{0} Offense Gem".format(tier)
    return cmp_string

def format_mob(mob):
    """
    mob (dict)
    """
    mob_string = "Name: {}\n" \
                 "Level: {}\n\n" \
                 "HP: {}\n" \
                 "HP Pad: {}\n" \
                 "Overall HP: {}\n\n" \
                 "Physical Resist: {}\n" \
                 "Magic Resist: {}\n" \
                 "Defense: {}\n\n" \
                 "Evasion: {}\n" \
                 "Crit Evasion: {}" \
                 .format(mob["id"], mob["level"], mob["hp"], mob["hp_padding"],
                         mob["hp"] + mob["hp_padding"], mob["phys_resist"],
                         mob["mag_resist"], mob["def"], mob["eva"],
                         mob["crit_eva"])
    return mob_string

def format_crit_chance(crit_chance):
    crit_chance *= 100
    crit_chance = round(crit_chance, 1)
    return "The critical hit chance is {} %".format(crit_chance)

def format_multiplier(multiplier):
    multiplier = round(multiplier, 1)
    return "The multiplier is {}".format(multiplier)
