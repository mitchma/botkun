import json
import os
import psycopg2
import psycopg2.pool

from src.util import log

DATABASE_URL = None
STATS_ID_COLUMN = None
STATS_VAL_COLUMN = None
TABLE = None
postgreSQL_pool = None

def init():
    global DATABASE_URL
    global STATS_ID_COLUMN
    global STATS_VAL_COLUMN
    global TABLE
    global postgreSQL_pool
    
    DATABASE_URL = os.environ["DATABASE_URL"]
    STATS_ID_COLUMN = os.environ["STATS_ID_COLUMN"]
    STATS_VAL_COLUMN = os.environ["STATS_VAL_COLUMN"]
    TABLE = os.environ["TABLE"]
    postgreSQL_pool = psycopg2.pool.SimpleConnectionPool(1, 20, DATABASE_URL)

def get(stats_id):
    # log("database::get(): Getting character stats with ID '{}'"
    #     .format(stats_id))
    stats_id = str(stats_id)
    ps_connection = postgreSQL_pool.getconn()
    ps_cursor = ps_connection.cursor()
    query = "SELECT {} FROM \"{}\" WHERE {} LIKE '{}'".format(STATS_VAL_COLUMN,
            TABLE, STATS_ID_COLUMN, stats_id)
    ps_cursor.execute(query)
    stats = ps_cursor.fetchone()
    ps_connection.commit()
    ps_cursor.close()
    postgreSQL_pool.putconn(ps_connection)
    if stats is None:
        # msg = "database::get(): Character stats with ID '{}' do not exist" \
        #       .format(stats_id)
        # log(msg)
        msg = "Character stats do not exist"
        raise Exception(msg)
    return stats[0]

def set(stats_id, stats):
    # log("database::set(): Upserting character stats with ID '{}'"
    #     .format(stats_id))
    stats_id = str(stats_id)
    stats = json.dumps(stats)
    ps_connection = postgreSQL_pool.getconn()
    ps_cursor = ps_connection.cursor()
    query = """INSERT INTO "{0}" ({1}, {2})
               VALUES ('{3}', '{4}')
               ON CONFLICT ({1}) DO UPDATE
               SET {2} = excluded.{2}""" \
            .format(TABLE, STATS_ID_COLUMN, STATS_VAL_COLUMN, stats_id, stats)
    ps_cursor.execute(query)
    ps_connection.commit()
    ps_cursor.close()
    postgreSQL_pool.putconn(ps_connection)

def remove(stats_id):
    # log("database::remove(): Removing character stats with ID '{}'"
    #     .format(stats_id))
    stats_id = str(stats_id)
    ps_connection = postgreSQL_pool.getconn()
    ps_cursor = ps_connection.cursor()
    query = "DELETE FROM \"{}\" WHERE {} LIKE '{}'" \
            .format(TABLE, STATS_ID_COLUMN, stats_id)
    ps_cursor.execute(query)
    ps_connection.commit()
    ps_cursor.close()
    postgreSQL_pool.putconn(ps_connection)
