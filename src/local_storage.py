import json
import os
from src.util import log

stats_dict = {}
CACHE_DIR = "cache"
CACHE_FILE = "save_cache.json"

def get(stats_id):
    log("local_storage::get(): Retrieving character stats with ID '{}'"
        .format(stats_id))
    stats_id = str(stats_id)
    try:
        stats = stats_dict[stats_id]
    except:
        # msg = "local_storage::get(): Character stats with ID '{}' do not " \
        #       "exist".format(stats_id)
        msg = "Character stats do not exist"
        raise Exception(msg)
    return stats

def set(stats_id, stat):
    log("local_storage::set(): Setting character stats with ID '{}'".format(stats_id))
    stats_id = str(stats_id)
    stats_dict[stats_id] = stat
    cache()

def remove(stats_id):
    log("local_storage::remove(): Removing character stats with ID '{}'"
        .format(stats_id))
    stats_id = str(stats_id)
    if stats_id not in stats_dict:
        # msg = "local_storage::get(): Character stats with ID '{}' do not " \
        #       "exist".format(stats_id)
        msg = "Character stats do not exist"
        raise Exception(msg)
    del stats_dict[stats_id]
    cache()

def restore():
    global stats_dict
    log("local_storage::restore(): Attempting to restore from cache")
    dir_name = os.path.join(os.getcwd(), CACHE_DIR)
    if os.path.exists(dir_name):
        with open(os.path.join(dir_name, CACHE_FILE), "r") as f:
            log("local_storage::restore(): Restored from cache")
            stats_dict = json.load(f)
    else:
        log("local_storage::restore(): No cache exists")

def cache():
    """
    Very naive implementation that does not handle race conditions.
    """
    log("local_storage::cache(): Attempting to cache")
    dir_name = os.path.join(os.getcwd(), CACHE_DIR)
    if not os.path.exists(dir_name):
        try:
            os.makedirs(dir_name)
        except OSError as e:
            log("local_storage::cache(): {}"
                .format(e))
            if e.errno != e.EEXIST:
                raise Exception("Cannot create directory")

    with open(os.path.join(dir_name, CACHE_FILE), "w") as f:
        f.write(json.dumps(stats_dict, indent=4))
        f.close()
        log("local_storage::cache(): Successfully cached")
