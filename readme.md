# BotKun #

A simple yet flexible optimizer for MS2.

# How to add the Bot to your Discord #
If you would like to run a local version or host your own version of the Bot, skip this section.
Simply click the following and add it to a server you manage and ONLY allow it the messaging permission:
https://discordapp.com/oauth2/authorize?client_id=523975233145929740&scope=bot&permissions=2048.
Read the `Usage` section below.

# Offline Mode [WIP] #
Note that this is NOT a Discord Bot. It simply runs the optimizer through CMD or shell by reading commands in a `.txt` file.

Install Python 3.6.7 (3.7+ breaks `discord.py`) and the dependencies `pip install -r requirements.txt` (may need to use `pip3`). Open up cmd/shell in the main directory and run `python3 botkun_offline.py`. This will run the commands in `botkun_offline.txt`. Feel free to edit it and add new commands to the file. There's also a `botkun_offline.bat` file availble for Windows user that will simply execute the python script.
TODO: integrate new commands such as crit_chance

# Local Discord Bot #
I assume you know what you're doing. Simply update the `token` value in `config.json` and you're good to go. If not, there are many great YouTube videos that will help you set up a Discord Bot.
You should probably also change how `local_storage.py` works if you're running the Bot locally (see Limitations section). One way is to use the code in `database.py` and attach that to a local database. 

# Heroku Discord Bot #
I use the free version along with a PostgreSQL database. Add the following to your App's Config Vars:

- `TOKEN`: Discord Bot's token. Do not let others know this.

- `DATABASE_URL`: database URL

- `TABLE`: name of table

- `STATS_ID_COLUMN`: Name of stats ID column

- `STATS_VAL_COLUMN`: Name of stats value column

Now create a database on Heroku, add the table, set the schema (stats ID is `str` and stats is `json`), and set the ID as the primary key.
Deploy onto Heroku, and add the Bot to the server.

# Usage #
First, invoke `!help` to view all the commands.

You would first want to grab a stats template via the command `!template`. Copy the output into a text editor and update the values with those from the ingame advanced stats page. Now paste that back into Discord and `!set` your character stats. You can confirm the stats by invoking `!stats`, which would provide you with some additional information such as the amount of bonus attack from pets, your critical hit chance, and the multiplier (used as a part of the damage formula, higher is better). The critical hit chance and multiplier values can also be viewed using the `!crit_chance` and `!multiplier` commands. If you would like to update a single value (such as piercing) in the future, you do not need to input the entire template. Simply set the stats that are updated:
```
!set
piercing: 26
wep_rarity: legendary
wep_effect: panic_double
job: striker
```

Once the stats are set, you are ready to use the other commands. Use `!help` to view the commands and `!help <COMMAND_NAME>` to view more detailed explanations for the specific commands.

The main feature of the Bot is the `compare` command that will give a percentage difference in the `multiplier` value based on the stat combinations you provide. You can use `!help compare` to see some examples. I'll use one of the examples mentioned in the Bot.

Suppose you have a 1.3% piercing and 3% boss damage Absolute Necklace with a tier 3 offense gem and two empty sockets equipped. You want to see if you should put some gems into the Absolute Necklace or switch to a Kandura's Pendant instead. Then you would use the following command:
```
!compare
kandura_pendant: true, piercing: -1.3, boss_dmg: -3, main_stat_gem: -3
main_stat_gem: 7, offense_gem: 5
```
and get an output similar to:
```
This is your base stats
Dmg % Diff:          0.0 %
Multiplier:    7286072.9

Combination 1
kandura_pendant: true, piercing: -1.3, boss_dmg: -3, main_stat_gem: -3
Dmg % Diff:         -2.4 %
Multiplier:    7114631.2

Combination 2
main_stat_gem: 7, offense_gem: 5
Dmg % Diff:          6.2 %
Multiplier:    7737094.4
```

Each line in `!compare` is one combination of stats. `kandura_pendant: true` means I'm now wearing a Kandura's Pendant, while the negative `piercing`, `boss_dmg`, `main_stat_gem` means I'm losing 1.3% piercing, 3% boss damage, and a tier 3 main stat gem from replacing the Absolute Necklace. The second line simply means I'm putting a tier 7 main stat gem and a tier 5 offense gem into the Absolute Necklace that I have currently equipped. The output first shows my base stats and its multiplier, then shows the change in damage and new multiplier for the combinations I want to compare. This command is very flexible, and can be used to compare the change due to pet, stats, weapon rarity, weapon effect (Acreon, Panic, Murp, MSL, etc.), and much more. Definitely take a look at the `!help compare` command.

Another useful tool is the `!info` command whose parameters can be seen from `!help info`. This provides some info such as credits, Bot limitations, and topics often discussed by the community. For example, invoking `!info what_neck` will discuss about Kandura's and Absolute and `!info what_gem` will compare offense and main stat gems.

# Limitations #
The caching code in `local_storage.py` is very naive and probably does not handle race conditions when used with Discord. Not sure how the database version performs since I've never tested concurrency.
Expect a lot of bugs since I made this to be extremely flexible while not having tested anything.

The formula and data used in the calculations are the result of tests by others on various different servers. While some values are datamined (which may differ depending on region), others are based on assumptions. For example, I do not fully know if `damage total percent` is additive or multiplicative with other percentage-based damages or if Kandura's 3% physical/magic attack increase is additive or multiplicative with Varrekant's Horns' active effect. For now, `damage total percent` is additive but can be set to multiplicative while the other is hard-coded to be additive.
Accuracy is not considered since I do not know the formula.
Only offensive stats are considered currently. Attack speed, a great stat for certain classes such as Heavy Gunners, is not considered due to the sheer complexity involved. Skill animation speeds and attack speed formulas are known and can be datamined, but there are just too many factors to consider.

# Credit #
- u/gaffox for the damage calculator on [Reddit](https://www.reddit.com/r/MapleStory2/comments/a1nj5j/damage_calculator/).
- u/Rhygrass for the gearing guide [Reddit](https://www.reddit.com/comments/9x2kff).
- u/TrylessDoer for the critical chance formula on [Reddit](https://www.reddit.com/r/MapleStory2/comments/a36vrq/datamined_critical_rate_algorithm/) and the [GitHub](https://timebomb.github.io/ms2-dmg-calc/) project on the Comparison Calculator under TimeBomb.
- sztupy for the Lua 5.1 decompiler on [Github](https://github.com/sztupy/luadec51).
- [Meowuniverse](https://www.meowuniverse.com/stats) for formulas and data.
- Miyuyami for the unpacker on [Github](https://github.com/Miyuyami/MS2Tools).
- Wunkolo for the unpacker on [Github](https://github.com/Wunkolo/Maple2Tools).