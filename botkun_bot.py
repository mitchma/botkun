import discord
import json
import os
import src.controller as controller
import src.formatter as formatter
import src.program_data as program_data
import src.storage as storage
import src.util as util

from discord.ext import commands
from src.util import log

client = commands.Bot(command_prefix = "!")

def token():
    """
    Retrieves token from config or Heroku environment vars.
    return (str, bool): token string and is_database
    """
    log("bot_kun::token(): Retrieving token")
    curr_token = ""
    config_name = "config.json"
    
    # Running locally
    if os.path.exists(config_name):
        with open(os.path.join(os.getcwd(), config_name), "r") as f:
            log("bot_kun::token(): Reading 'config.json'")
            curr_token = json.load(f)
            curr_token = curr_token["token"]
            if curr_token:
                log("bot_kun::token(): Token is '{}'".format(curr_token))
                return curr_token, False
            log("bot_kun::token(): 'config.json' has no token")

    # Running on Heroku
    log("bot_kun::token(): Retrieving from environment variable 'TOKEN'")
    curr_token = os.environ["TOKEN"]

    if not curr_token:
        msg = "Please configure your Discord Bot token"
        raise Exception(msg)

    log("bot_kun::token(): Token is '{}'".format(curr_token))
    return curr_token, True

@client.event
async def on_ready():
    log("botkun_bot::on_ready(): Bot-Kun is ready.")

@client.event
async def on_command_error(error, ctx):
    if isinstance(error, discord.ext.commands.errors.CommandNotFound):
        return
    log(error)
    msg = "{}. Please refer to `!help`.".format(error)
    msg = msg.replace("Command raised an exception: Exception: ", "")
    msg = formatter.discord_wrap(msg, is_error = True,
        mentions = ctx.message.mentions)
    await client.send_message(ctx.message.channel, msg)

@client.command(pass_context = True)
async def set(ctx):
    stats_id = ctx.message.author.id
    log("botkun_bot::set(): Called by {} (ID: {})"
        .format(ctx.message.author, stats_id))
    log("Input content:\n{}".format(ctx.message.content))

    if ctx.message.content.strip() == "!set" or \
            ctx.message.content.split('\n')[0].strip() != "!set":
        log("botkun_bot::set(): Missing or bad parameter(s)")
        msg = "Please provide the stats to be updated on a new line. For " \
              "example:\n!set\npiercing: 14.5\npm_piercing: 13.5"
        msg = formatter.discord_wrap(msg, is_error = True,
            mentions = ctx.message.mentions)
        await client.reply(msg)
        return

    inputs = ctx.message.content
    inputs = inputs[inputs.find('\n') + 1:]
    
    try:
        controller.set(stats_id, inputs)
    except Exception as e:
        msg = formatter.discord_wrap(e, is_error = True,
            mentions = ctx.message.mentions)
        await client.reply(msg)
    else:
        msg = "Your stats have been set. Use the `!stats` command to view " \
              "your new stats."
        msg = formatter.discord_wrap(msg, mentions = ctx.message.mentions)
        await client.reply(msg)

@client.command(pass_context = True)
async def remove(ctx):
    stats_id = ctx.message.author.id
    log("botkun_bot::remove(): Called by {} (ID: {})"
        .format(ctx.message.author, stats_id))
    log("Input content:\n{}".format(ctx.message.content))
    controller.remove(stats_id)
    msg = "Your stats have been removed."
    msg = formatter.discord_wrap(msg, mentions = ctx.message.mentions)
    await client.reply(msg)

@client.command(pass_context = True)
async def compare(ctx):
    stats_id = ctx.message.author.id
    log("botkun_bot::compare(): Called by {} (ID: {})"
        .format(ctx.message.author, stats_id))
    log("Input content:\n{}".format(ctx.message.content))

    if ctx.message.content.strip() == "!compare" or \
            ctx.message.content.split('\n')[0].strip() != "!compare":
        log("botkun_bot::compare(): Missing or bad parameter(s)")
        msg = "Please provide stat combinations as parameter(s) on a new " \
              "line. For example:\n!compare\npiercing: 1, boss_dmg: 2\n" \
              "pm_piercing: 3"
        msg = formatter.discord_wrap(msg, is_error = True,
            mentions = ctx.message.mentions)
        await client.reply(msg)
        return

    inputs = ctx.message.content
    inputs = inputs[inputs.find('\n') + 1:]
    try:
        comparisons = controller.compare(stats_id, inputs)
    except Exception as e:
        msg = formatter.discord_wrap(e, is_error = True,
            mentions = ctx.message.mentions)
        await client.reply(msg)
    else:
        cmp_string = formatter.format_compare(comparisons)
        cmp_string = formatter.discord_wrap(cmp_string,
            mentions = ctx.message.mentions)
        log("Output content:\n{}".format(cmp_string))
        await client.reply(cmp_string)

@client.command(pass_context = True)
async def crit_chance(ctx, stats_id = None):
    log("botkun_bot::crit_chance(): Called by {} (ID: {})"
        .format(ctx.message.author, ctx.message.author.id))
    log("Input content:\n{}".format(ctx.message.content))
    stats_id = util.disc_id(stats_id) if stats_id else ctx.message.author.id
    crit_chance = controller.crit_chance(stats_id)
    crit_chance_string = formatter.format_crit_chance(crit_chance)
    crit_chance_string = formatter.discord_wrap(crit_chance_string,
        mentions = ctx.message.mentions)
    log("Output content:\n{}".format(crit_chance_string))
    await client.reply(crit_chance_string)

client.remove_command("help")
@client.command(pass_context = True)
async def help(ctx, command = ""):
    log("botkun_bot::help(): Called by {} (ID: {})"
        .format(ctx.message.author, ctx.message.author.id))
    log("Input content:\n{}".format(ctx.message.content))

    if command == "":
        msg = program_data.help_help
    elif command == "compare":
        msg = program_data.help_compare
    elif command == "crit_chance":
        msg = program_data.help_crit_chance
    elif command == "help":
        msg = program_data.help_help_help
    elif command == "info":
        msg = program_data.help_info
    elif command == "mob":
        msg = program_data.help_mob
    elif command == "multiplier":
        msg = program_data.help_multiplier
    elif command == "opt_attr":
        msg = program_data.help_opt_attr
    elif command == "opt_gems":
        msg = program_data.help_opt_gems
    elif command == "rant":
        msg = program_data.help_rant
    elif command == "remove":
        msg = program_data.help_remove
    elif command == "set":
        msg = program_data.help_set
    elif command == "stats":
        msg = program_data.help_stats
    elif command == "template":
        msg = program_data.help_template
    else:
        msg = "'{}' is not a valid command.".format(command, is_error = True)
    msg = formatter.discord_wrap(msg, mentions = ctx.message.mentions)
    await client.say(msg)

@client.command(pass_context = True)
async def info(ctx, command = ""):
    log("botkun_bot::info(): Called by {} (ID: {})"
        .format(ctx.message.author, ctx.message.author.id))
    log("Input content:\n{}".format(ctx.message.content))

    if command == "credit":
        msg = program_data.info_credit
    elif command == "compare_key":
        msg = program_data.info_compare_key
    elif command == "dim_ret":
        msg = program_data.info_dim_ret
    elif command == "limitation":
        msg = program_data.info_limitation
    elif command == "wep_effect":
        msg = program_data.info_wep_effect
    elif command == "what_cape":
        msg = program_data.info_cape
    elif command == "what_gem":
        msg = program_data.info_gem
    elif command == "what_neck":
        msg = program_data.info_neck
    elif command == "what_wep":
        msg = program_data.info_wep
    else:
        msg = "'{}' is not a valid argument. `!help` for more details" \
              .format(command, is_error = True)
    msg = formatter.discord_wrap(msg, mentions = ctx.message.mentions)
    await client.say(msg)

@client.command(pass_context = True)
async def mob(ctx, mob_id = ""):
    log("botkun_bot::mob(): Called by {} (ID: {})"
        .format(ctx.message.author, ctx.message.author.id))
    log("Input content:\n{}".format(ctx.message.content))
    try:
        mob = controller.mob_info(mob_id)
    except Exception as e:
        msg = "{}. `!help mob` for more details.".format(e)
        msg = formatter.discord_wrap(msg, is_error = True,
            mentions = ctx.message.mentions)
        await client.reply(msg)
    else:
        mob_string = formatter.format_mob(mob)
        mob_string = formatter.discord_wrap(mob_string,
            mentions = ctx.message.mentions)
        await client.reply(mob_string)

@client.command(pass_context = True)
async def multiplier(ctx, stats_id = None):
    log("botkun_bot::multiplier(): Called by {} (ID: {})"
        .format(ctx.message.author, ctx.message.author.id))
    log("Input content:\n{}".format(ctx.message.content))
    stats_id = util.disc_id(stats_id) if stats_id else ctx.message.author.id
    multiplier = controller.multiplier(stats_id)
    multiplier_string = formatter.format_multiplier(multiplier)
    multiplier_string = formatter.discord_wrap(multiplier_string,
        mentions = ctx.message.mentions)
    log("Output content:\n{}".format(multiplier_string))
    await client.reply(multiplier_string)

@client.command(pass_context = True)
async def opt_attr(ctx, attr_pts = "0"):
    stats_id = ctx.message.author.id
    log("botkun_bot::opt_attr(): Called by {} (ID: {})"
        .format(ctx.message.author, stats_id))
    log("Input content:\n{}".format(ctx.message.content))
    try:
        distribution = controller.optimize_attributes(stats_id, attr_pts)
    except Exception as e:
        msg = formatter.discord_wrap(e, is_error = True,
            mentions = ctx.message.mentions)
        await client.reply(msg)
    else:
        dis_string = formatter.format_optimal_attributes(distribution)
        dis_string = formatter.discord_wrap(dis_string)
        log("Output content:\n{}".format(dis_string))
        await client.reply(dis_string)

@client.command(pass_context = True)
async def opt_gems(ctx):
    stats_id = ctx.message.author.id
    log("botkun_bot::opt_gems(): Called by {} (ID: {})"
        .format(ctx.message.author, stats_id))
    log("Input content:\n{}".format(ctx.message.content))

    if ctx.message.content.strip() == "!opt_gems" or \
            ctx.message.content.split('\n')[0].strip() != "!opt_gems":
        log("botkun_bot::opt_gems(): Missing or bad parameter(s)")
        msg = "Please provide the number of gems to socket (0 to 9) " \
              "as well as the optional offset stats. For example:\n" \
              "!opt_gems\namount: 6\ntier: 8\noffset: main_stat_gem: -4, " \
              "offense_gem: -7, b_atk: -12"
        msg = formatter.discord_wrap(msg, is_error = True,
            mentions = ctx.message.mentions)
        await client.reply(msg)
        return

    inputs = ctx.message.content
    inputs = inputs[inputs.find('\n') + 1:]

    try:
        gem_set = controller.optimize_gems(stats_id, inputs)
    except Exception as e:
        msg = formatter.discord_wrap(e, is_error = True,
            mentions = ctx.message.mentions)
        await client.reply(msg)
    else:
        gem_string = formatter.format_optimal_gems(gem_set)
        gem_string = formatter.discord_wrap(gem_string)
        log("Output content:\n{}".format(gem_string))
        await client.reply(gem_string)

@client.command(pass_context = True)
async def rant(ctx):
    stats_id = ctx.message.author.id
    log("botkun_bot::rant(): Called by {} (ID: {})"
        .format(ctx.message.author, stats_id))
    log("Input content:\n{}".format(ctx.message.content))
    rant = controller.rant()
    rant = formatter.discord_wrap(rant,
        mentions = ctx.message.mentions)
    await client.reply(rant)

@client.command(pass_context = True)
async def stats(ctx, stats_id = None):
    stats_id = util.disc_id(stats_id) if stats_id else ctx.message.author.id
    log("botkun_bot::stats(): Called by {} (ID: {})"
        .format(ctx.message.author, stats_id))
    log("Input content:\n{}".format(ctx.message.content))
    stats = controller.stats(stats_id)
    stats = formatter.format_stats(stats)
    stats = formatter.discord_wrap(stats,
        mentions = ctx.message.mentions)
    log("Output content:\n{}".format(stats))
    await client.reply(stats)

@client.command(pass_context = True)
async def template(ctx):
    log("botkun_bot::template(): Called by {} (ID: {})"
        .format(ctx.message.author, ctx.message.author.id))
    log("Input content:\n{}".format(ctx.message.content))
    msg = controller.template()
    msg = formatter.discord_wrap(msg,
        mentions = ctx.message.mentions)
    await client.reply(msg)

if __name__ == "__main__":
    bot_token, is_database = token()
    storage.toggle(is_database)
    storage.init()
    client.run(bot_token)
