import os
import src.controller as controller
import src.formatter as formatter
import src.util as util

FILE_NAME = "botkun_offline.txt"
CHAR_ID = "offline_user"

def read_offline_text():
    file_path = os.path.join(os.getcwd(), FILE_NAME)
    try:
        with open(file_path, "r") as f:
            line = f.read()
    except Exception:
        log("Failed to open '{}'".format(FILE_NAME))
        raise
    return line

def parse_offline_text(lines):
    def is_command_line(line):
        for command in ("help", "template", "set", "compare", "stats", "opt_attr"):
            pass
    commands = []
    command, inputs = None, ""

    for line in lines.split("\n"):
        line = line.lower().strip()
        if line == "" or line[0] == "#":
            continue
        elif line in ("set", "compare", "stats", "optimize_attributes"):
            if command:
                commands.append({
                    "command": command,
                    "inputs": inputs
                })
                command, inputs = line, ""
            else:
                command = line
        else:
            if command:
                inputs += "{}\n".format(line)
    else:
        commands.append({
            "command": command,
            "inputs": inputs,
        })
    return commands

def execute_commands(commands):
    for pair in commands:
        command, inputs = pair["command"], pair["inputs"]
        print("{}\nCommand: {}\n".format('-' * 50, command))

        if command == "set":
            controller.set(CHAR_ID, inputs)
        elif command == "compare":
            comparisons = controller.compare(CHAR_ID, inputs)
            cmp_string = formatter.format_compare(comparisons)
            print(cmp_string)
        elif command == "stats":
            stats = controller.stats(CHAR_ID)
            stats_string = formatter.format_stats(stats)
            print(stats_string)
        elif command == "optimize_attributes":
            opt_attr = controller.optimize_attributes(CHAR_ID, inputs)
            opt_attr_string = formatter.format_optimal_attributes(opt_attr)
            print(opt_attr_string)

if __name__ == "__main__":
    util.toggle_log()
    lines = read_offline_text()
    commands = parse_offline_text(lines)
    execute_commands(commands)
